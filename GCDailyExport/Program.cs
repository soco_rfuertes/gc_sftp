﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SSISExporter
{
    class Program
    {
        static void Main(string[] args)
        {
            //user: golfcanada
            //pw: vaKoIxp7p5
            //host: sftp.inboxmarketer.com
            //Destination Folder: Inbound
            //port: 22
            try
            {

                long gcExportfileSize;

                try
                {
                    //get file filesize
                    gcExportfileSize = new System.IO.FileInfo(Properties.Settings.Default.FileFullPath).Length;
                }
                catch (Exception)
                {
                    Console.WriteLine("Could not get the filesize, No file found..");
                    gcExportfileSize = 0;
                }

                WinSCP winscp = new WinSCP();
                //winscp.SendSimpleEmail(Properties.Settings.Default.SendToEmails, "Test email", " Remote Sync Report" + DateTime.Now.ToString(), "");
                winscp.HostName = Properties.Settings.Default.HostName;
                winscp.UserName = Properties.Settings.Default.UserName;
                winscp.Password = Properties.Settings.Default.Password;
                winscp.PortNumber = Properties.Settings.Default.PortNumber;
                winscp.SshHostKeyFingerprint = Properties.Settings.Default.SshHostKeyFingerprint;
                winscp.localPathWithFilename = Properties.Settings.Default.localPath;
                //winscp.PutFiles();

                if (winscp.PutFiles())
                {
                    //send email
                    winscp.SendSimpleEmail(Properties.Settings.Default.SendToEmails, Properties.Settings.Default.EmailText.Replace("<FileSize>", string.Format("<{0}KB>", gcExportfileSize.ToString())), Properties.Settings.Default.EmailSubject, "");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("" + ex.Message);
                Console.ReadLine();
                
            }
        }
    }
}
