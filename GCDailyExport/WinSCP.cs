﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WinSCP;
using System.Threading;
using System.IO;
using System.Globalization;
using System.Net.Mail; //Namespace 

namespace SSISExporter
{
    public class WinSCP
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int PortNumber { get; set; }
        public string SshHostKeyFingerprint { get; set; }
        public string localPathWithFilename { get; set; }
  
        public bool PutFiles()
        {
            bool ret = false;
            try
            {
                DateTime dt = DateTime.Now;

                string[] files = Directory.GetFiles(Properties.Settings.Default.localPath + "/");
                FileCodePageConverter converter = new FileCodePageConverter();
                converter.SetCulture("en-US");
                foreach (string file in files)
                {
                    converter.Convert(file, file, "Windows-1252"); // Convert from code page Windows-1250 to UTF-8  
                } 

                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = HostName,
                    UserName = UserName,
                    Password = Password,
                    PortNumber = PortNumber,
                    SshHostKeyFingerprint = SshHostKeyFingerprint
                };
                using (Session session = new Session())
                {
                    session.ExecutablePath = Properties.Settings.Default.WinSCPLocation;
                    // Connect
                    session.Open(sessionOptions);

                    //var localPathFile = session.HomePath + "/inbound" + @"/GCDailyExport.txt";

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
       
                    transferOptions.TransferMode = TransferMode.Binary; //The Transfer Mode - 

                    transferOptions.FilePermissions = null; //Permissions applied to remote files; 

                    transferOptions.PreserveTimestamp = true; //Set last write time of 

                    // Throw on any error
                    transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;

                    transferOptions.FileMask = "*>=1D";

                    TransferOperationResult transferResult;

                    Console.WriteLine("Uploading file to remote server...");

                    //the parameter list is: local Path, Remote Path, Delete source file?, transfer Options  
                    transferResult = session.PutFiles(Properties.Settings.Default.localPath, session.HomePath + "/inbound", false, transferOptions);

                    Thread.Sleep(3000);
                    Console.WriteLine("Upload successfull...");
                    Thread.Sleep(3000);

                    // delete GCDailyExport file
                    Console.WriteLine("Deleting GCDailyExport.txt file...");
                    if (File.Exists(Properties.Settings.Default.localPath + @"\GCDailyExport.txt"))
                    {
                        File.Delete(Properties.Settings.Default.localPath + @"\GCDailyExport.txt");
                    }
                    Thread.Sleep(3000);
                    Console.WriteLine("GCDailyExport.txt file deleted...");

                    transferResult.Check();
                    Thread.Sleep(6000);
                }

                ret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                Console.ReadLine();
                ret = false;
            }
            return ret;
        }

        private class FileCodePageConverter
        {
            public void Convert(string path, string path2, string codepage)
            {
                byte[] buffer = File.ReadAllBytes(path);
                if (buffer[0] != 0xef && buffer[0] != 0xbb)
                {
                    byte[] buffer2 = Encoding.Convert(Encoding.GetEncoding(codepage), Encoding.UTF8, buffer);
                    byte[] utf8 = new byte[] { 0xef, 0xbb, 0xbf };
                    FileStream fs = File.Create(path2);
                    fs.Write(utf8, 0, utf8.Length);
                    fs.Write(buffer2, 0, buffer2.Length);
                    fs.Close();
                }
            }

            public void SetCulture(string name)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(name);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(name);
            }
        }

        public void SendSimpleEmail(string EmailAddress, string HtmlBody, string Subject, string AttachmentPath)
        {
            Console.WriteLine("Sending SyncReport Email...");
            //============================================================
            // GET Mail  CREDENTIALS
            //============================================================

            //tmpRecpient.Email 
            //Send Email 
            System.Net.Mail.MailMessage MyMailMessage = new System.Net.Mail.MailMessage();

            //From requires an instance of the MailAddress type"

            MyMailMessage.From = new System.Net.Mail.MailAddress(Properties.Settings.Default.smtpUser, "SyncReport Notifications");

            // Add Read Reciept Headers
            //============================================================================================================
            //
            //MyMailMessage.Headers.Add("Disposition-Notification-To", "<jaymeh@xitechnologies.com>")
            //-------------------------------------------------------------------------------------------------------------

            //add all the email recepients
            foreach (var emailAddress in EmailAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                MyMailMessage.To.Add(emailAddress);
            }

            MyMailMessage.Subject = Subject;

            MyMailMessage.Body = HtmlBody;
            MyMailMessage.IsBodyHtml = true;

            if (!string.IsNullOrEmpty(AttachmentPath))
            {
                MyMailMessage.Attachments.Add(new System.Net.Mail.Attachment(AttachmentPath));
            }
            //=========================================================================================================
            // Send the Email
            //=========================================================================================================
            System.Net.Mail.SmtpClient SMTPServer = new System.Net.Mail.SmtpClient();
            SMTPServer.UseDefaultCredentials = true;
            SMTPServer.Port = Convert.ToInt32(Properties.Settings.Default.smtpPort);
            //Hard Coded Server Port
            SMTPServer.Host = Properties.Settings.Default.smtpHost;
            SMTPServer.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.smtpUser, Properties.Settings.Default.smtpPassword);
            SMTPServer.EnableSsl = true;

            try
            {
                SMTPServer.Send(MyMailMessage);
                Thread.Sleep(3000);
                Console.WriteLine("SyncReport Email Sent...");
                Thread.Sleep(6000);
                //MessageBox.Show(ex.Message)
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
    }
}
